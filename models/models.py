# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Model_proxy(models.TransientModel):
    _name = 'multiexport.model_proxy'

    name = fields.Char("Model")
    parents = fields.Many2many('ir.model')
    model_id = fields.Many2one('ir.model', compute="_get_model_id")

    wizard = fields.Many2one("multiexport.export_wizard", ondelete="cascade")

    def _get_model_id(self):
        for rec in self:
            rec.model_id = self.env['ir.model'].search([('model', '=', rec.name)])

    def stop(self):
        parent_ids = [self.model_id.id]
        while True:
            children = self.search([('wizard','=',self.wizard.id), ('parents', 'in', parent_ids)])
            for child in children: child.parents = child.parents - self.env['ir.model'].browse(parent_ids)
            unparented = self.search([('wizard','=',self.wizard.id), ('parents','=',False)])
            if not unparented: break
            parent_ids = unparented.ids
            self.wizard.models = self.wizard.models - unparented
            #unparented.unlink()
        return {
            "type": "set_scrollTop",
        }


class ExportWizard(models.TransientModel):
    _name = 'multiexport.export_wizard'
    _description = "Wizard to select stop-models"

    def collect_models_recursive(self, model_name, model_names, parent):
        if model_name in model_names: return model_names[model_name].append(parent)
        model_names[model_name] = [parent]
        model = self.env[model_name]
        fields = model.fields_get()
        stored_fields = [key for key in fields if 'depends' in fields[key] and not fields[key]['depends'] and fields[key].get('store', False)]
        m2o_fields = [key for key in stored_fields if fields[key]['type'] == 'many2one']
        o2m_fields = [key for key in stored_fields if fields[key]['type'] == 'one2many']
        m2m_fields = [key for key in stored_fields if fields[key]['type'] == 'many2many']
        for key in m2o_fields: self.collect_models_recursive(fields[key]['relation'], model_names, model_name)
        for key in o2m_fields: self.collect_models_recursive(fields[key]['relation'], model_names, model_name)
        for key in m2m_fields: self.collect_models_recursive(fields[key]['relation'], model_names, model_name)

    def collect_models(self):
        model_names = {}
        self.collect_models_recursive(self.env.context['default_model'], model_names, 'root')
        new_vals = [{
            'name': model,
            'parents': [(6, 0, self.env['ir.model'].search([('model', 'in', model_names[model])]).ids)],
        } for model in model_names]
        return self.env['multiexport.model_proxy'].create(new_vals)

    models = fields.One2many('multiexport.model_proxy', "wizard", default=collect_models)
    model = fields.Char()
    model_ids = fields.Char()

    def apply(self):
        pass


# class multiexport(models.Model):
#     _name = 'multiexport.multiexport'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100