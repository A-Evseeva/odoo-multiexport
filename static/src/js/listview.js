 odoo.define('multiexport.list', function (require) {
    "use strict";
    var core = require('web.core');

    var ListController = require('web.ListController');
    var rpc = require('web.rpc');
    var session = require('web.session');
    var framework = require('web.framework');
    var crash_manager = require('web.crash_manager');
    var _t = core._t;

    ListController.include({
        renderButtons: function ($node) {
            this._super.apply(this, arguments);
            if (this.$buttons) {
                this.$multiexport_button = this.$buttons.find('.multi-export');
                this.$multiexport_button.click(this.proxy('multiexport'));
                this.$multiimport_button = this.$buttons.find('.multi-import');
                this.$multiimport_button.click(this.proxy('multiimport'));
            }
        },
        init: function () {
            this._super.apply(this, arguments);
            this.is_admin = session.is_system;
        },
        multiexport: function() {
            if (this.selectedRecords.length == 0) return alert("No records selected");
            var action = {
                    type: 'ir.actions.act_window',
                    res_model: 'multiexport.export_wizard',
                    view_mode: 'form',
                    view_type: 'form',
                    views: [[false, 'form']],
                    target: 'new',
                    context: {default_model: this.modelName, default_model_ids: this.getSelectedIds()},
            };
            this.do_action(action);

        },
        multiimport: function() {
            console.log('multi-import');
        },
    });

    var core = require('web.core');
    var Widget= require('web.Widget');
    var widgetRegistry = require('web.widget_registry');
    var FieldManagerMixin = require('web.FieldManagerMixin');

    var MyWidget = Widget.extend(FieldManagerMixin, {
        init: function (parent, model, state) {
            this._super(parent);
            FieldManagerMixin.init.call(this);
            this.models = model.data.models;
            this.initModel = model.data.model;
            this.initIds = model.data.model_ids;
        },
        start: function () {
             var button = this.$el.append("<button class='export_button'>EXPORT</button>");
        },
        events: {
            "click button.export_button": "export",
        },
        export: function(evt){
            var modelNames = [];
            for (var i in this.models.data) modelNames.push(this.models.data[i].data);

            framework.blockUI();
            this.getSession().get_file({
                url: '/multiexport/export',
                data: {data: JSON.stringify({
                    model: this.initModel,
                    ids: eval(this.initIds),
                    models: modelNames,
                })},
                complete: framework.unblockUI,
                error: crash_manager.rpc_error.bind(crash_manager),
            });
            /**/
        },
    });

    widgetRegistry.add(
        'export_button', MyWidget
    );
});