# -*- coding: utf-8 -*-
from odoo import http
from odoo.tools.misc import str2bool, xlwt, file_open
import io, json, operator

class Multiexport(http.Controller):
    @http.route('/multiexport/export', type='http', auth="user")
    def export(self, data, token):
        #TODO: check if user is admin
        params = json.loads(data)
        model, ids, models = operator.itemgetter('model', 'ids', 'models')(params)
        models = {each['name']: {'ids':[], 'parents': each['parents']['res_ids'], 'name': each['name'], 'model_id': each['model_id']['res_id']} for each in models}

        workbook = xlwt.Workbook()
        self.collect_models(model, ids, models)
        for modelName in models:
            self.model2xls(modelName, models[modelName].get('ids', []), workbook)

        fp = io.BytesIO()
        workbook.save(fp)
        fp.seek(0)
        xldata = fp.read()
        fp.close()

        return http.request.make_response(xldata, headers=[
            ('Content-Disposition', http.content_disposition(model + '.xls')),
            ('Content-Type', 'application/vnd.ms-excel')],
             cookies={'fileToken': token})

    def collect_models(self, model, ids, models_list):
        if not models_list.get(model, False): return
        models_list[model]['ids'] = list(set(models_list[model]['ids'] + ids))
        children = [m for m in models_list if models_list[model]['model_id'] in model_list[m]['parents']]
        if not children: return

        Model = http.request.env[model]

        fields = Model.fields_get()
        stored_fields = [key for key in fields if 'depends' in fields[key] and not fields[key]['depends'] and fields[key].get('store', False)]
        m2o_fields = [key for key in stored_fields if fields[key]['type'] == 'many2one']
        o2m_fields = [key for key in stored_fields if fields[key]['type'] == 'one2many']
        m2m_fields = [key for key in stored_fields if fields[key]['type'] == 'many2many']
        found_models = []
        for key in m2o_fields:
            related = fields[key]['relation']
            related_ids = set([record[key].id for record in Model.sudo().browse(ids) if record[key]])
            if related in models_list: related_ids = related_ids - set(models_list[related]['ids'])
            if len(related_ids): found_models.append({'model': related, 'ids':list(related_ids)})
        for key in o2m_fields:
            related = fields[key]['relation']
            if 'relation_field' in fields[key]:
                related_ids = set(http.request.env[related].sudo().search([(fields[key]['relation_field'], 'in', ids)]).ids)
                if related in models_list: related_ids = related_ids - set(models_list[related]['ids'])
                if len(related_ids): found_models.append({'model': related, 'ids': list(related_ids)})
        for key in m2m_fields:
            related = fields[key]['relation']
            related_ids = set(Model.browse(ids).mapped(key).ids)
            if related in models_list: related_ids = related_ids - set(models_list[related]['ids'])
            if len(related_ids): found_models.append({'model': related, 'ids': list(related_ids)})

        for found_model in found_models:
            self.collect_models(found_model['model'], found_model['ids'], models_list)

    def model2xls(self, model, ids, workbook):
        worksheet = workbook.add_sheet(model)
        Model = http.request.env[model]
        fields = Model.fields_get()
        filtered_fields = [key for key in fields if 'depends' in fields[key] and not fields[key]['depends'] and fields[key].get('store', False) and not key == 'id' and not fields[key]['type'] in ['binary']]
        records = Model.browse(ids)

        worksheet.write(0, 0, 'id')
        for i in range(len(filtered_fields)): worksheet.write(0, i+1, filtered_fields[i])

        row_index = 1
        for record in records:
            worksheet.write(row_index, 0, record.id)
            for i in range(len(filtered_fields)):
                value = record[filtered_fields[i]]
                if fields[filtered_fields[i]]['type'] == 'many2one': value = value.id
                if fields[filtered_fields[i]]['type'] in ['many2many', 'one2many']: value = ', '.join(value.mapped(lambda x:str(x.id)))
                #TODO: also interpret dates and times and maybe something else
                worksheet.write(row_index, i+1, value)
            row_index += 1



#     @http.route('/multiexport/multiexport/', auth='public')
#     def index(self, **kw):
#         return "Hello, world"

#     @http.route('/multiexport/multiexport/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('multiexport.listing', {
#             'root': '/multiexport/multiexport',
#             'objects': http.request.env['multiexport.multiexport'].search([]),
#         })

#     @http.route('/multiexport/multiexport/objects/<model("multiexport.multiexport"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('multiexport.object', {
#             'object': obj
#         })